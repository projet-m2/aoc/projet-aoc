# Projet AOC

## Equipe M2 ILA

 - Victor HARABARI
 - Nathanaël TOUCHARD

## Environnement

 - Java 11
 - Maven
 - JUnit

## Maven
### Compilation du projet
```bash
  mvn compile
  ```
### Lancement des tests
```bash
  mvn test
  ```
### Génération de l'exécutable
```bash
  mvn package
  ```
  Pour ne pas exécuter les tests :
  ```bash
  mvn package -DskipTests
  ```
### Execution
```bash
  java -jar target/projet-AOC-1.0-SNAPSHOT.jar
  ```
## Introduction
L'objectif du projet est d'appliquer le patron de conception Active Object vu en cours. Ce projet permettra la diffusion par des canaux de transmission vers des afficheurs, de un flot de valeurs capté par un capteur (ici les données diffusées seront une séquence croissante d’entiers). De plus, l'intérêt est de pouvoir choisir différentes stratégies de diffusions telles que la diffusion Atomique, Séquentielle et par Epoque.

## Architecture

Voici l'organisation du projet :

![Architecture projet](images/architecture-projet.png)

- javadoc : javadoc générée selon le code source.
- src/main/java : code source du projet
    * afficheur : classe pour un afficheur synchrone.
    * capteur : architecture d'un capteur synchrone.
    * observer : interfaces pour les observers et sujet.
    * proxy : classes pour implémenter le pattern proxy.
    * strategy : les différentes strategies de diffusions implémentées selon le pattern strategy. 
- src/test/java : tests unitaires
- target : classes générées et exécutable.

## Choix techniques

**Modèle M1 :**
Il s'agit d'opération asynchrone avec 1 thread par objet.
Le modèle M1 est composé du pattern Observer, Proxy et Strategy, tout ça avec une communication asynchrone.

### Observer
Pour le patron de conception observer, nous l'avons employé de deux manière, un Observer synchrone et un Observer asynchrone.

![Pattern observer](images/pattern-observer.png)

### Proxy
Le patron Proxy est implémenté par le Canal, qui peut être Capteur et Afficheur. Il joue un double rôle du coté synchrone et asynchrone.
- Un canal du point de vue d'un capteur synchrone est un observer asynchrone par l'implémentation de ObserverAsyncDeCapteur.
- Un canal du point de vue d'un afficheur synchrone est un capteur asynchrone par l'implémentation de CapteurAsync. 

![Pattern proxy](images/pattern-proxy.png)

### Strategy
Le patron de conception Strategy permet de choisir la stratégie de diffusion en fonction d'un context.
Dans ce projet l'interface AlgoDiffusion permet de spécifier les méthodes communes à chaque implémentation (atomique, séquentielle, époque).

![Pattern strategy](images/pattern-strategy.png)

**Les différentes diffusions :**

 - atomique = tous les observateurs reçoivent la même valeur, qui est celle du capteur.
 - séquentielle = tous les observateurs reçoivent la même valeur, mais la séquence peut être différente de la séquence réelle du capteur.
 - époque = aucune coordination de la part du capteur, on laisse les afficheurs se débrouiller entre eux pour régler les divergences entre les valeurs qu'ils lisent. La seule aide apportée par le capteur est l'estampille de temps mise sur la valeur courante du capteur retournée par getValue().

### Afficheur
Pour l'afficheur nous sommes resté dans une optique simple. Il s'agit d'un afficheur en console, en affichant l'identifiant de l'afficheur et la valeur capté.

### Active Object

**Modèle M2 :**

![Diagramme M2](images/m2-active-object.png)

Pour le **modèle M3**, il s'agit du M2 en remplaçant par les classes Java qui correspondent.

 - MethodInvocation &rarr; java.util.concurrent.Callable
 - Scheduler &rarr; java.util.concurrent.ScheduledExecutorService;
 - Future &rarr; java.util.concurrent.Future;
 - ActiveQueue &rarr; Rien
 - Update et GetValue &rarr; lambdas conforme à Callable
 
 Le reste ne change pas.

**Diagrammes de séquences :**

Partie 1 :

![Diagramme M2](images/diagramme-sequence-1.png)

Partie 2 :

![Diagramme M2](images/diagramme-sequence-2.png)

Partie 3 :

![Diagramme M2](images/diagramme-sequence-3.png)


## Finalités
### Etat du projet
Le projet est fonctionnel pour chaque stratégies de diffusions.

Il s'agit de 100 tick effectués avec 5 afficheurs (ID:id). On écrit qu'un morceau de la diffusion car la liste est longue.

**Exemple diffusion atomique :**
```
Rentrer le numéro ou type de diffusion souhaité [numéro - type] :
1 - ATOMIQUE / 2 - SEQUENTIEL / 3 - EPOQUE
1
-------ATOMIQUE-------
[ID:3] ~ Capted value : 1
[ID:5] ~ Capted value : 1
[ID:4] ~ Capted value : 1
[ID:1] ~ Capted value : 1
[ID:2] ~ Capted value : 1
[ID:1] ~ Capted value : 2
[ID:3] ~ Capted value : 2
[ID:5] ~ Capted value : 2
[ID:2] ~ Capted value : 2
[ID:4] ~ Capted value : 2
[ID:5] ~ Capted value : 3
[ID:4] ~ Capted value : 3
[ID:1] ~ Capted value : 3
[ID:2] ~ Capted value : 3
[ID:3] ~ Capted value : 3
[ID:4] ~ Capted value : 4
[ID:2] ~ Capted value : 4
[ID:1] ~ Capted value : 4
[ID:5] ~ Capted value : 4
[ID:3] ~ Capted value : 4
[ID:5] ~ Capted value : 5
[ID:4] ~ Capted value : 5
[ID:1] ~ Capted value : 5
[ID:3] ~ Capted value : 5
[ID:2] ~ Capted value : 5
```

**Exemple diffusion séquentielle :**

```
Rentrer le numéro ou type de diffusion souhaité [numéro - type] :
1 - ATOMIQUE / 2 - SEQUENTIEL / 3 - EPOQUE
2
-------SEQUENTIEL-------
[ID:4] ~ Capted value : 1
[ID:1] ~ Capted value : 1
[ID:5] ~ Capted value : 1
[ID:3] ~ Capted value : 1
[ID:2] ~ Capted value : 1
[ID:2] ~ Capted value : 6
[ID:3] ~ Capted value : 6
[ID:4] ~ Capted value : 6
[ID:1] ~ Capted value : 6
[ID:5] ~ Capted value : 6
[ID:5] ~ Capted value : 11
[ID:1] ~ Capted value : 11
[ID:3] ~ Capted value : 11
[ID:4] ~ Capted value : 11
[ID:2] ~ Capted value : 11
[ID:1] ~ Capted value : 16
[ID:4] ~ Capted value : 16
[ID:3] ~ Capted value : 16
[ID:2] ~ Capted value : 16
[ID:5] ~ Capted value : 16
[ID:5] ~ Capted value : 21
[ID:4] ~ Capted value : 21
[ID:2] ~ Capted value : 21
[ID:1] ~ Capted value : 21
[ID:3] ~ Capted value : 21
[ID:4] ~ Capted value : 26
[ID:2] ~ Capted value : 26
[ID:5] ~ Capted value : 26
[ID:3] ~ Capted value : 26
[ID:1] ~ Capted value : 26
```
**Exemple diffusion par époque :**

```
Rentrer le numéro ou type de diffusion souhaité [numéro - type] :
1 - ATOMIQUE / 2 - SEQUENTIEL / 3 - EPOQUE
3
-------EPOQUE-------
[ID:1] ~ Capted value : 3
[ID:1] ~ Capted value : 3
[ID:3] ~ Capted value : 3
[ID:2] ~ Capted value : 4
[ID:5] ~ Capted value : 4
[ID:4] ~ Capted value : 4
[ID:2] ~ Capted value : 4
[ID:3] ~ Capted value : 4
[ID:5] ~ Capted value : 4
[ID:5] ~ Capted value : 5
[ID:3] ~ Capted value : 5
[ID:1] ~ Capted value : 5
[ID:5] ~ Capted value : 6
[ID:4] ~ Capted value : 6
[ID:2] ~ Capted value : 6
[ID:1] ~ Capted value : 6
[ID:2] ~ Capted value : 6
[ID:4] ~ Capted value : 7
[ID:4] ~ Capted value : 7
[ID:4] ~ Capted value : 7
[ID:5] ~ Capted value : 7
[ID:1] ~ Capted value : 7
[ID:1] ~ Capted value : 7
[ID:1] ~ Capted value : 7
[ID:3] ~ Capted value : 7
[ID:3] ~ Capted value : 7
[ID:2] ~ Capted value : 9
[ID:5] ~ Capted value : 9
[ID:4] ~ Capted value : 9
[ID:3] ~ Capted value : 9
[ID:4] ~ Capted value : 9
[ID:5] ~ Capted value : 10
[ID:2] ~ Capted value : 10
[ID:4] ~ Capted value : 10
[ID:1] ~ Capted value : 10
[ID:4] ~ Capted value : 10
[ID:5] ~ Capted value : 10
[ID:5] ~ Capted value : 10
[ID:5] ~ Capted value : 11
[ID:3] ~ Capted value : 11
[ID:1] ~ Capted value : 11
[ID:2] ~ Capted value : 11
[ID:1] ~ Capted value : 12
[ID:3] ~ Capted value : 12
[ID:3] ~ Capted value : 12
[ID:2] ~ Capted value : 12
```

### Tests

Les tests passent tous, ils testent les méthodes des classes du projets mais aussi l'appel à certaines méthode lors du workflow.

![Tests](images/tests.png)
