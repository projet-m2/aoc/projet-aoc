package afficheur;

import capteur.Capteur;
import capteur.CapteurImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import proxy.Canal;
import strategy.DiffusionAtomique;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class AfficheurTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Mock
    private Afficheur afficheur;

    private Canal canal;

    private Capteur capteur;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        System.setOut(new PrintStream(outContent));
        canal = new Canal();
        capteur = new CapteurImpl();
        canal.register(afficheur);
        capteur.register(canal);
        capteur.setDiffusionStrategy(new DiffusionAtomique());
    }

    @Test
    public void updateIsCalledTest() {
        capteur.tick();
        Mockito.verify(afficheur).update(canal);
    }

    @Test
    public void displayTest(){
        Afficheur afficheurTest = new Afficheur(1);
        canal.register(afficheurTest);
        capteur.tick();
        assertEquals("[ID:1] ~ Capted value : 1\n", outContent.toString());
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }
}
