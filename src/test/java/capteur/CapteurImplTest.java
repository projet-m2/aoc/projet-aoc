package capteur;

import observer.ObserverAsync;
import org.junit.Before;
import org.junit.Test;
import proxy.Canal;
import strategy.DiffusionAtomique;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CapteurImplTest {

    private CapteurImpl capteur;

    @Before
    public void setup(){
        this.capteur = new CapteurImpl();
    }

    @Test
    public void getValue() {
        assertTrue(this.capteur.getValue() == 0);
    }

    @Test
    public void tick() {
        this.capteur.setDiffusionStrategy(new DiffusionAtomique());
        this.capteur.tick();
        assertTrue(this.capteur.getValue() == 1);
    }

    @Test
    public void setValue() {
        this.capteur.setValue(6);
        assertTrue(this.capteur.getValue() == 6);
    }

    @Test
    public void register() {
        Canal canal = new Canal();
        this.capteur.register(canal);
        assertTrue(this.capteur.getObservers().contains(canal));
    }

    @Test
    public void deregister() {
        Canal canal = new Canal();
        this.capteur.register(canal);
        this.capteur.deregister(canal);
        assertTrue(this.capteur.getObservers().isEmpty());
    }

    @Test
    public void setDiffusionStrategy() {
        DiffusionAtomique diffusion = new DiffusionAtomique();
        this.capteur.setDiffusionStrategy(diffusion);
        assertSame(this.capteur.getAlgoDiffusion(), diffusion);
    }

    @Test
    public void getObservers() {
        List<ObserverAsync> observers = new ArrayList<>();
        Canal canal = new Canal();
        observers.add(canal);
        this.capteur.register(canal);
        assertEquals(observers, this.capteur.getObservers());
    }

    @Test
    public void getAlgoDiffusion() {
        DiffusionAtomique diffusion = new DiffusionAtomique();
        this.capteur.setDiffusionStrategy(diffusion);
        assertSame(diffusion, this.capteur.getAlgoDiffusion());
    }
}
