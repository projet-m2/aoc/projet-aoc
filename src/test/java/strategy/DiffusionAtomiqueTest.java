package strategy;

import capteur.Capteur;
import capteur.CapteurImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class DiffusionAtomiqueTest {

    @Mock
    DiffusionAtomique diffusion;

    private Capteur capteur;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        capteur = new CapteurImpl();
    }

    @Test
    public void configureTest() {
        capteur.setDiffusionStrategy(diffusion);
        Mockito.verify(diffusion).configure(capteur);
    }

    @Test
    public void executeTest() {
        capteur.setDiffusionStrategy(diffusion);
        capteur.tick();
        Mockito.verify(diffusion).execute();
    }
}
