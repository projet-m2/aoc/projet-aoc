package proxy;

import afficheur.Afficheur;
import capteur.Capteur;
import capteur.CapteurImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import strategy.DiffusionAtomique;

import static org.junit.Assert.assertTrue;

public class UpdateTest {


    private Update update;

    private Afficheur afficheur;

    private Canal canal;

    private Capteur capteur;

    @Before
    public void setup(){
        afficheur = new Afficheur(1);
        canal = new Canal();
        capteur = new CapteurImpl();
        canal.register(afficheur);
        capteur.register(canal);
        update = new Update(afficheur, canal);
        capteur.setDiffusionStrategy(new DiffusionAtomique());
        capteur.tick();
    }

    @Test
    public void call() throws Exception {
        assertTrue(update.call() == null);
    }
}
