package proxy;

import afficheur.Afficheur;
import capteur.Capteur;
import capteur.CapteurImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import strategy.DiffusionAtomique;

import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertTrue;

public class CanalTest {

    private Canal canal;

    private Afficheur afficheur;

    private Capteur capteur;

    @Before
    public void setup() {
        canal = new Canal();
        afficheur = new Afficheur(1);
        capteur = new CapteurImpl();
    }

    @Test
    public void register() {
        canal.register(afficheur);
    }

    @Test
    public void deregister() throws ExecutionException, InterruptedException {
        canal.register(afficheur);
        canal.deregister(afficheur);
    }

    @Test
    public void update() throws ExecutionException, InterruptedException {
        canal.register(afficheur);
        capteur.register(canal);
        capteur.setDiffusionStrategy(new DiffusionAtomique());
        capteur.tick();
        assertTrue(canal.update(capteur).get() == null);
    }

    @Test
    public void getValue() throws ExecutionException, InterruptedException {
        canal.register(afficheur);
        capteur.register(canal);
        capteur.setDiffusionStrategy(new DiffusionAtomique());
        capteur.tick();
        assertTrue(canal.getValue().get() == 1);
    }
}
