package proxy;

import capteur.CapteurImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertTrue;

public class GetValueTest {

    @Mock
    private GetValue getValue;

    @Before
    public void setup() {
        getValue = new GetValue(new CapteurImpl());
    }

    @Test
    public void call() {
        assertTrue(this.getValue.call() == 0);
    }
}
