package capteur;

import observer.ObserverAsync;

/**
 * Interface d'un observable par asynchronicité.
 *
 * @param <T> observer asynchrone
 * @author Victor Harabari, Nathanael Touchard
 */
public interface ObservableByAsync<T extends ObserverAsync> {

    /**
     * Enregistre un observer asynchrone du capteur.
     *
     * @param observer observer à associer
     */
    void register(ObserverAsync observer);

    /**
     * Efface un observer asynchrone du capteur.
     *
     * @param observer observer à effacer
     */
    void deregister(ObserverAsync observer);
}
