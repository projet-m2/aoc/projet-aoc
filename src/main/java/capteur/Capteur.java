package capteur;

import java.util.List;

import observer.ObserverAsync;
import strategy.interfaces.AlgoDiffusion;

/**
 * Interface d'un capteur synchrone.
 *
 * @author Victor Harabari, Nathanael Touchard
 */
public interface Capteur extends ObservableByAsync {

    /**
     * Retourne la valeur du capteur.
     *
     * @return valeur du capteur
     */
    int getValue();

    /**
     * Permet de choisir une valeur au capteur.
     *
     * @param value valeur à assigner au capteur
     */
    void setValue(int value);

    /**
     * Incrémente la valeur (le compteur) du capteur de 1.
     */
    void tick();

    /**
     * Permet de changer l'algorithme de diffusion et configurer le capteur lié à la diffusion.
     *
     * @param algoDiffusion algorithme de diffusion choisi
     */
    void setDiffusionStrategy(AlgoDiffusion algoDiffusion);

    /**
     * Permet de connaître la liste des observer attachés à ce capteur.
     *
     * @return la liste des observer
     */
    List<ObserverAsync> getObservers();
}
