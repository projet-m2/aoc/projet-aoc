package capteur;

import observer.ObserverAsync;
import strategy.interfaces.AlgoDiffusion;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation d'un capteur synchrone.
 *
 * @author Victor Harabari, Nathanael Touchard
 */
public class CapteurImpl implements Capteur {

    /**
     * Valeur courante du capteur.
     */
    private Integer value;

    /**
     * Liste des observer du capteur.
     */
    private List<ObserverAsync> observers = new ArrayList<>();

    /**
     * Algorithme de diffusion qui notifie les observateurs.
     */
    private AlgoDiffusion algoDiffusion;

    /**
     * Constructeur d'un capteur, la valeur capté au départ est 0.
     */
    public CapteurImpl() {
        this.value = 0;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public void tick() {
        this.value += 1;
        algoDiffusion.execute();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public void register(ObserverAsync observer) {
        observers.add(observer);
    }

    @Override
    public void deregister(ObserverAsync observer) {
        observers.remove(observer);
    }


    @Override
    public void setDiffusionStrategy(AlgoDiffusion algoDiffusion) {
        this.algoDiffusion = algoDiffusion;
        this.algoDiffusion.configure(this);
    }

    @Override
    public List<ObserverAsync> getObservers() {
        return observers;
    }

    /**
     * Permet de connaître l'algorithme de diffusion courant.
     *
     * @return l'algorithme de diffusion courant
     */
    public AlgoDiffusion getAlgoDiffusion() {
        return algoDiffusion;
    }


}
