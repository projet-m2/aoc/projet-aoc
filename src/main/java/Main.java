import afficheur.Afficheur;
import capteur.CapteurImpl;
import capteur.Capteur;
import proxy.Canal;
import strategy.DiffusionAtomique;
import strategy.DiffusionEpoque;
import strategy.DiffusionSequentielle;

import java.util.Scanner;

/**
 * Application créant un capteur, des canaux et des afficheurs.
 *
 * @author Victor Harabari, Nathanael Touchard
 */
public class Main {

    /**
     * Nombre d'afficheurs souhaités.
     */
    private static final int NB_AFFICHEUR = 5;

    /**
     * Nombre d'incrémentation effectué sur le capteur.
     */
    private static final int NB_TICK = 100;

    /*
    Message en console.
     */
    private static final String MESSAGE_ALGO = "Rentrer le numéro ou type de diffusion souhaité [numéro - type] :";
    private static final String MESSAGE_ALGO_CHOICE = "1 - ATOMIQUE / 2 - SEQUENTIEL / 3 - EPOQUE";
    private static final String MESSAGE_ALGO_CHOICE_1 = "1";
    private static final String MESSAGE_ALGO_CHOICE_ATOMIQUE = "ATOMIQUE";
    private static final String MESSAGE_ALGO_CHOICE_2 = "2";
    private static final String MESSAGE_ALGO_CHOICE_SEQUENTIEL = "SEQUENTIEL";
    private static final String MESSAGE_ALGO_CHOICE_3 = "3";
    private static final String MESSAGE_ALGO_CHOICE_EPOQUE = "EPOQUE";

    public static void main(String[] args) {
        // Création du capteur
        Capteur capteur = new CapteurImpl();

        // Création des afficheurs et des canaux.
        for (int i = 1; i <= NB_AFFICHEUR; i++) {
            Afficheur afficheur = new Afficheur(i);

            Canal canal = new Canal();
            canal.register(afficheur);

            capteur.register(canal);
        }

        Scanner scanner = new Scanner(System.in);
        System.out.println(MESSAGE_ALGO);

        // Choix de l'algorithme de diffusion
        boolean hasToChoice = true;
        while (hasToChoice) {
            System.out.println(MESSAGE_ALGO_CHOICE);
            String input = scanner.nextLine();
            if (MESSAGE_ALGO_CHOICE_1.equals(input) || MESSAGE_ALGO_CHOICE_ATOMIQUE.equals(input)) {
                System.out.println("-------" + MESSAGE_ALGO_CHOICE_ATOMIQUE + "-------");
                capteur.setDiffusionStrategy(new DiffusionAtomique());
                hasToChoice = false;
            } else if (MESSAGE_ALGO_CHOICE_2.equals(input) || MESSAGE_ALGO_CHOICE_SEQUENTIEL.equals(input)) {
                System.out.println("-------" + MESSAGE_ALGO_CHOICE_SEQUENTIEL + "-------");
                capteur.setDiffusionStrategy(new DiffusionSequentielle());
                hasToChoice = false;
            } else if (MESSAGE_ALGO_CHOICE_3.equals(input) || MESSAGE_ALGO_CHOICE_EPOQUE.equals(input)) {
                System.out.println("-------" + MESSAGE_ALGO_CHOICE_EPOQUE + "-------");
                capteur.setDiffusionStrategy(new DiffusionEpoque());
                hasToChoice = false;
            }
        }

        // Incrémentation de la valeur du capteur
        for (int i = 0; i < NB_TICK; i++) {
            capteur.tick();
        }
    }

}
