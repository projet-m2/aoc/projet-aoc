package observer;

import java.util.concurrent.ExecutionException;

/**
 * Interface d'un observer synchrone de sujet asynchrone.
 *
 * @param <T> sujet asynchrone
 * @author Victor Harabari, Nathanael Touchard
 */
public interface ObserverDeSubjectAsync<T extends SubjectAsync> {

    /**
     * Permet la mise à jour d'un observer par un sujet asynchrone l'ayant notifié.
     *
     * @param s sujet asynchrone
     * @throws ExecutionException
     * @throws InterruptedException
     */
    void update(T s) throws ExecutionException, InterruptedException;
}
