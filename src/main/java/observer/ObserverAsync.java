package observer;

import capteur.ObservableByAsync;

import java.util.concurrent.Future;

/**
 * Interface d'un observer asynchrone.
 *
 * @param <T> observable asynchrone
 * @author Victor Harabari, Nathanael Touchard
 */
public interface ObserverAsync<T extends ObservableByAsync> {
    /**
     * Permet de notifier un observable asynchrone de l'état d'un sujet.
     *
     * @param s sujet
     * @return un Future représentant le résultat d'un calcul asynchrone
     */
    Future<Integer> update(T s);
}
