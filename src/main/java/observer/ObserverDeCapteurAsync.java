package observer;

import proxy.interfaces.CapteurAsync;

/**
 * Interface d'un observer synchrone de capteur asynchrone.
 * Ceci est une spécialisation de l'interface ObserverDeSubjectAsync<T>
 *
 * @author Victor Harabari, Nathanael Touchard
 */
public interface ObserverDeCapteurAsync extends ObserverDeSubjectAsync<CapteurAsync> {

}
