package observer;

/**
 * Interface d'un sujet asychrone.
 *
 * @param <T> observer de sujet asynchrone
 * @author Victor Harabari, Nathanael Touchard
 */
public interface SubjectAsync<T extends ObserverDeSubjectAsync> {

    /**
     * Enregistre un observer asynchrone du sujet.
     *
     * @param observer observer asynchrone
     */
    void register(T observer);

    /**
     * Efface un observer asynchrone du sujet.
     *
     * @param observer observer asynchrone
     */
    void deregister(T observer);
}
