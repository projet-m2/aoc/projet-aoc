package proxy;

import capteur.Capteur;

import java.util.concurrent.Callable;

/**
 * Classe asynchrone qui implémente Callable. Un Integer est mis en attente et invoqué par le scheduler. Il permet l'appel a la valeur du capteur synchrone depuis le coté asynchrone.
 *
 * @author Victor Harabari, Nathanael Touchard
 */
public class GetValue implements Callable<Integer> {

    /**
     * Capteur synchrone pour appeler la valeur.
     */
    private Capteur capteur;

    /**
     * Constructeur du callable GetValue.
     *
     * @param capteur capteur synchrone
     */
    public GetValue(Capteur capteur) {
        this.capteur = capteur;
    }

    /**
     * Appel synchrone au capteur une fois que le Callable est invoqué par le scheduler.
     *
     * @return la valeur du capteur
     */
    @Override
    public Integer call() {
        return this.capteur.getValue();
    }

}
