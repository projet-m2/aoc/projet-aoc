package proxy;

import java.util.Random;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import capteur.Capteur;
import observer.ObserverDeCapteurAsync;
import proxy.interfaces.CapteurAsync;
import proxy.interfaces.ObserverAsyncDeCapteur;

/**
 * Interface d'un canal, qui joue un double rôle du coté synchrone et asynchrone.
 * Un canal du point de vue d'un capteur synchrone est un observer asynchrone par l'implémentation de ObserverAsyncDeCapteur.
 * Un canal du point de vue d'un afficheur synchrone est un capteur asynchrone par l'implémentation de CapteurAsync.
 *
 * @author Victor Harabari, Nathanael Touchard
 */
public class Canal implements CapteurAsync, ObserverAsyncDeCapteur {

    /**
     * Ensemble de threads disponibles pour l'execution asynchrone entre le capteur synchrone et l'afficheur synchrone.
     */
    private ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(10);

    /**
     * Observer de capteur asynchrone.
     */
    private ObserverDeCapteurAsync observer;

    /**
     * Capteur qui sera rendu asynchrone par le canal.
     */
    private Capteur capteur;

    /**
     * Constructeur d'un canal.
     */
    public Canal() {
    }

    /**
     * Enregistre un observer synchrone au capteur asynchrone qui est le canal.
     */
    @Override
    public void register(ObserverDeCapteurAsync observer) {
        this.observer = observer;
    }

    /**
     * Efface un observer synchrone au capteur asynchrone qui est le canal.
     */
    @Override
    public void deregister(ObserverDeCapteurAsync observer) {
        this.observer = null;
    }

    /**
     * Schedule (mise en attente) l'appel au Update de l'observer synchrone pour rendre l'appel à l'observer asynchrone du point de vue du capteur.
     * Le delai est compris entre 0 et 3 secondes.
     *
     * @return un Future représentant un Integer d'un calcul asynchrone (ici null)
     */
    @Override
    public Future<Integer> update(Capteur capteur) {
        this.capteur = capteur;
        final int delai = new Random().nextInt(3000);

        Update update = new Update(this.observer, this);
        return this.executorService.schedule(update, delai, TimeUnit.MILLISECONDS);
    }

    /**
     * Schedule (mise en attente) l'appel au GetValue du capteur synchrone pour rendre l'appel au capteur asynchrone du point de vue de l'observer.
     * Le delai est compris entre 0 et 3 secondes.
     *
     * @return un Future représentant un résultat contenant la valeur d'un calcul asynchrone
     */
    @Override
    public Future<Integer> getValue() {
        final int delai = new Random().nextInt(3000);

        GetValue getValue = new GetValue(this.capteur);
        return this.executorService.schedule(getValue, delai, TimeUnit.MILLISECONDS);
    }

}
