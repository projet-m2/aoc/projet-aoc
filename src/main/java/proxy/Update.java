package proxy;

import java.util.concurrent.Callable;

import observer.ObserverDeCapteurAsync;
import proxy.interfaces.CapteurAsync;

/**
 * Classe asynchrone qui implémente Callable. Un Integer est mis en attente et invoqué par le scheduler. Il permet de notifier l'observer synchrone depuis le coté asynchrone.
 *
 * @author Victor Harabari, Nathanael Touchard
 */
public class Update implements Callable<Integer> {

    /**
     * Observer synchrone de capteur asynchrone pour notifier le changement de valeur.
     */
    private ObserverDeCapteurAsync afficheur;

    /**
     * Capteur asynchrone, l'observer pourra obtenir la valeur modifiée.
     */
    private CapteurAsync canal;

    /**
     * Constructeur du callable Update.
     *
     * @param afficheur observer synchrone de capteur
     * @param canal     capteur asynchrone
     */
    public Update(ObserverDeCapteurAsync afficheur, CapteurAsync canal) {
        this.afficheur = afficheur;
        this.canal = canal;
    }

    /**
     * Notification synchrone sur l'observer de capteur une fois que le Callable est invoqué par le scheduler.
     *
     * @return null
     * @throws Exception
     */
    public Integer call() throws Exception {
        this.afficheur.update(this.canal);
        return null;
    }

}
