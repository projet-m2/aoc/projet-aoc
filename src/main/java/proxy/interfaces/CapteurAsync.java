package proxy.interfaces;

import java.util.concurrent.Future;

import observer.ObserverDeCapteurAsync;
import observer.SubjectAsync;

/**
 * Interface d'un capteur asynchrone.
 *
 * @author Victor Harabari, Nathanael Touchard
 */
public interface CapteurAsync extends SubjectAsync<ObserverDeCapteurAsync> {

    /**
     * Permet de récupérer la valeur du capteur de manière asynchrone.
     *
     * @return un Future représentant un Integer d'un calcul asynchrone
     */
    Future<Integer> getValue();
}
