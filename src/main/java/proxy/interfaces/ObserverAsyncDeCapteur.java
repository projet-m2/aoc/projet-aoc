package proxy.interfaces;

import java.util.concurrent.Future;

import capteur.Capteur;
import observer.ObserverAsync;

/**
 * Observer asynchrone de capteur permettant la mise à jour d'un observer après
 * un certain délai.
 *
 * @author Victor Harabari, Nathanael Touchard
 */
public interface ObserverAsyncDeCapteur extends ObserverAsync<Capteur> {

    /**
     * Notifie un observer asynchrone de l'état d'un capteur synchrone
     *
     * @param capteur capteur synchrone
     * @return un Future représentant un Integer d'un calcul asynchrone
     */
    Future<Integer> update(Capteur capteur);

}
