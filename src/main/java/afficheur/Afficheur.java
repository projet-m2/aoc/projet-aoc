package afficheur;

import java.util.concurrent.ExecutionException;

import observer.ObserverDeCapteurAsync;
import proxy.interfaces.CapteurAsync;

/**
 * Implementation d'un afficheur synchrone.
 *
 * @author Victor Harabari, Nathanael Touchard
 */
public class Afficheur implements ObserverDeCapteurAsync {

    /**
     * Identifiant de l'afficheur.
     */
    private int id;

    /**
     * Constructeur de l'afficheur avec un identifiant donné.
     *
     * @param id identifiant choisi
     */
    public Afficheur(int id) {
        this.id = id;
    }

    /**
     * Permet à un afficheur de recuperer la valeur du capteur l'ayant notifié (ici un canal, donc un CapteurAsync).
     * Affiche la valeur dans la console avec l'identifiant de l'afficheur.
     */
    @Override
    public void update(CapteurAsync canal) {
        try {
            int value = canal.getValue().get();
            System.out.println("[ID:" + this.id + "] ~ Capted value : " + value);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
