package strategy;

import capteur.Capteur;
import strategy.interfaces.AlgoDiffusion;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

/**
 * Algorithme de notification des observers asynchrone du capteur basé sur la diffusion atomique.
 * Tous les observers sont notifiés de toutes les valeurs prises par le capteur. Celui-ci ne peut s'incrementer que quand tous les observateurs ont lu sa valeur.
 *
 * @author Victor Harabari, Nathanael Touchard
 */
public class DiffusionAtomique implements AlgoDiffusion {

    /**
     * Capteur synchrone sur lequel va s'appliquer la diffusion atomique.
     */
    private Capteur capteur;

    @Override
    public void configure(Capteur capteur) {
        this.capteur = capteur;
    }

    /**
     * Tous les observers sont notifiés. On stocke les Futures renvoyés par les notifications.
     * Tous Futures sont résolus, ce qui met en attente l'écriture dans le capteur qui attend le retour de cette méthode.
     */
    @Override
    public void execute() {
        List<Future> futureList = new ArrayList<>();
        capteur.getObservers().forEach(observer -> futureList.add(observer.update(capteur)));
        futureList.forEach(future -> {
            try {
                future.get();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

}
