package strategy.interfaces;

import capteur.Capteur;

/**
 * Interface d'un algorithme de diffusion des observers asynchrone d'un capteur.
 *
 * @author Victor Harabari, Nathanael Touchard
 */
public interface AlgoDiffusion {

    /**
     * Configure le capteur lié à la diffusion.
     *
     * @param capteur capteur à lier à l'algorithme de diffusion
     */
    void configure(Capteur capteur);

    /**
     * Notifie les observers selon le choix de l'algorithme diffusions.
     */
    void execute();

}
