package strategy;

import capteur.CapteurImpl;
import capteur.Capteur;
import observer.ObserverAsync;
import strategy.interfaces.AlgoDiffusion;

import java.util.HashMap;
import java.util.Map;

/**
 * Algorithme de notification des observers asynchrone du capteur basé sur la diffusion par époque.
 * Aucune coordination de la part du capteur, on laisse les afficheurs se débrouiller entre eux pour régler
 * les divergences entre les valeurs qu'ils lisent. La seule aide apportée par le capteur est l'estampille
 * de temps mise sur la valeur courante du capteur retournée par getValue().
 *
 * @author Victor Harabari, Nathanael Touchard
 */
public class DiffusionEpoque implements AlgoDiffusion {

    /**
     * Capteur synchrone sur lequel va s'appliquer la diffusion atomique.
     */
    private Capteur capteur;

    @Override
    public void configure(Capteur capteur) {
        this.capteur = capteur;
    }

    /**
     * Tous les observers sont notifiés. Les afficheurs se débrouillent entre eux pour les divergences entre les valeurs.
     */
    @Override
    public void execute() {
        this.capteur.getObservers().forEach(observer -> observer.update(this.capteur));
    }

}
