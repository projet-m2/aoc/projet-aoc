package strategy;

import capteur.CapteurImpl;
import capteur.Capteur;
import strategy.interfaces.AlgoDiffusion;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

/**
 * Algorithme de notification des observers asynchrone du capteur basé sur la diffusion séquentielle.
 * Tous les observers sont notifiés d'un ensemble de valeurs prise par le capteur. Celui-ci continue de s'incrementer.
 *
 * @author Victor Harabari, Nathanael Touchard
 */
public class DiffusionSequentielle implements AlgoDiffusion {

    /**
     * Capteur synchrone sur lequel va s'appliquer la diffusion atomique.
     */
    private Capteur capteur;

    /**
     * Liste de Future en attente renvoyés par les notifications.
     */
    private List<Future> waitings = new ArrayList<>();


    @Override
    public void configure(Capteur capteur) {
        this.capteur = capteur;
    }

    /**
     * Si les Future en attente sont résolu, clone le capteur courant et notifie les observers du capteur avec ce clone.
     */
    @Override
    public void execute() {
        if (this.waitings.stream().allMatch(future -> future.isDone())) {
            Capteur clone = new CapteurImpl();
            clone.setValue(this.capteur.getValue());
            this.capteur.getObservers().forEach(observer -> this.waitings.add(observer.update(clone)));
        }
    }

}
